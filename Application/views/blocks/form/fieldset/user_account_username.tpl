[{assign var="bCompatibleTheme" value=false}]
[{assign var="sCSSClassLabel" value=""}]
[{if $oViewConf|method_exists:'isFlowCompatibleTheme' && $oViewConf->isFlowCompatibleTheme()}]
    [{assign var="bCompatibleTheme" value=true}]
    [{assign var="sCSSClassLabel" value=$sCSSClassLabel|cat:"control-label "}]
[{/if}]
[{if $oViewConf|method_exists:'isWaveCompatibleTheme' && $oViewConf->isWaveCompatibleTheme()}]
    [{assign var="bCompatibleTheme" value=true}]
[{/if}]
[{if $oViewConf->getConfigParam('bTRWMailRepeatActive')}]
    [{if $bCompatibleTheme && $oViewConf->getConfigParam('bTRWMailRepeatUseBlocks')}]
        <label class="[{$sCSSClassLabel}] col-lg-3 req" for="userLoginName">[{oxmultilang ident="EMAIL_ADDRESS"}]</label>
        <div class="col-lg-9">
            <input id="userLoginName" class="form-control js-oxValidate js-oxValidate_notEmpty js-oxValidate_email" type="email" name="lgn_usr" value="[{$oView->getActiveUsername()}]" required="required" />
            <div class="help-block"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="[{$sCSSClassLabel}] col-lg-3 req" for="userLoginNameRepeat">[{oxmultilang ident="TRWMAILREPEAT_EMAIL_ADDRESS_REPEAT"}]</label>
        <div class="col-lg-9">
            <input id="userLoginNameRepeat" class="form-control js-oxValidate js-oxValidate_notEmpty js-oxValidate_email" type="email" name="lgn_usr_repeat" value="[{$oViewConf->getRequestParameter('lgn_usr_repeat')|strip_tags}]" required="required" />
            <div class="help-block">
                [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxusername}]
            </div>
        </div>
    [{else}]
        [{include file="message/error.tpl" statusMessage=TRWMAILREPEAT_BLOCKS_NOT_COMPATIBLE|oxmultilangassign}]
        [{$smarty.block.parent}]
    [{/if}]
    [{oxscript add="$(document).ready(function(){ $('#userLoginName, #userLoginNameRepeat').bind('copy paste cut',function(e){e.preventDefault();});});"}]
[{else}]
    [{$smarty.block.parent}]
[{/if}]
