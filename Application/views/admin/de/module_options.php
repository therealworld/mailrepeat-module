<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwmailrepeat' => 'Theme-Einstellungen',

    'SHOP_MODULE_bTRWMailRepeatActive'         => 'Mail Wiederholung aktivieren',
    'SHOP_MODULE_bTRWMailRepeatUseBlocks'      => 'Nutze die Template-Blöcke des Moduls ohne weitere individuelle Theme-Anpassungen',
    'SHOP_MODULE_bTRWMailRepeatValidEMailHost' => 'validiere zusätzlich den EMail Host (example@gmail.com - prüfe: "gmail.com")',
];
