<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'SHOP_MODULE_GROUP_trwmailrepeat' => 'Theme-Options',

    'SHOP_MODULE_bTRWMailRepeatActive'         => 'activate Mail Repeat',
    'SHOP_MODULE_bTRWMailRepeatUseBlocks'      => 'Use the template blocks of the module without any additional custom theme customizations',
    'SHOP_MODULE_bTRWMailRepeatValidEMailHost' => 'additional validate the EMail Host (example@gmail.com - check: "gmail.com")',
];
