<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\MailRepeatModule\Application\Component;

use Exception;
use OxidEsales\Eshop\Application\Model\User;
use OxidEsales\Eshop\Core\Exception\UserException;
use OxidEsales\Eshop\Core\Registry;

/**
 * User object manager.
 *
 * @mixin \OxidEsales\Eshop\Application\Component\UserComponent
 */
class UserComponent extends UserComponent_parent
{
    /**
     * OXID-Core.
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function createUser()
    {
        $oConfig = Registry::getConfig();
        $oRequest = Registry::getRequest();
        if ($oConfig->getConfigParam('bTRWMailRepeatActive')) {
            // collecting values to check
            // first EMail
            $sUser = $oRequest->getRequestParameter('lgn_usr');

            // second EMail
            $sUserRepeat = $oRequest->getRequestParameter('lgn_usr_repeat', true);

            try {
                $oUser = oxNew(User::class);
                $oUser->additionalEMailVaidation($sUser, $sUserRepeat);
            } catch (UserException $exception) {
                Registry::getUtilsView()->addErrorToDisplay($exception, false, true);

                return false;
            }
        }

        return parent::createUser();
    }
}
