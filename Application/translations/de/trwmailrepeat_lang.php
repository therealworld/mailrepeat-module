<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'TRWMAILREPEAT_BLOCKS_NOT_COMPATIBLE'              => 'Das Theme ist nicht zu "FLOW" oder "WAVE" kompatibel. Bitte deaktivieren Sie die Blocknutzung in den Moduleinstellungen und passen Ihr Template individuell an.',
    'TRWMAILREPEAT_EMAIL_ADDRESS_REPEAT'               => 'E-Mail-Adresse bestätigen',
    'TRWMAILREPEAT_ERROR_MESSAGE_EMAIL_DO_NOT_MATCH'   => 'Fehler: Die E-Mails stimmen nicht überein.',
    'TRWMAILREPEAT_ERROR_MESSAGE_EMAIL_HOST_NOT_VALID' => 'Fehler: Der E-Mail-Host <b>%s</b> ist ungültig.',
];
