<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [
    'charset' => 'UTF-8',

    'TRWMAILREPEAT_BLOCKS_NOT_COMPATIBLE'              => 'The theme is not compatible with "FLOW" or "WAVE". Please deactivate the block usage in the module settings and customize your template individually.',
    'TRWMAILREPEAT_EMAIL_ADDRESS_REPEAT'               => 'E-Mail-Adress repeat',
    'TRWMAILREPEAT_ERROR_MESSAGE_EMAIL_DO_NOT_MATCH'   => 'Error: E-Mails don\'t match.',
    'TRWMAILREPEAT_ERROR_MESSAGE_EMAIL_HOST_NOT_VALID' => 'Error: The E-Mail-Host <b>%s</b> is not valid.',
];
