<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\MailRepeatModule\Application\Model;

use OxidEsales\Eshop\Core\Exception\StandardException;
use OxidEsales\Eshop\Core\Registry;
use TheRealWorld\ToolsPlugin\Traits\DataGetter;

/**
 * User class.
 *
 * @mixin \OxidEsales\Eshop\Application\Model\User
 */
class User extends User_parent
{
    use DataGetter;

    /**
     * Checks if email (used as login) is not the same like the repeated Mail
     * valid.
     *
     * @param string $sEMail user email
     *
     * @throws StandardException
     */
    public function additionalEMailVaidation(string $sEMail, string $sEMailRepeat): void
    {
        $oInputValidator = Registry::getInputValidator();

        // checking email repeat
        $oInputValidator->checkEMailRepeat($sEMail, $sEMailRepeat);

        // checking email host
        if (Registry::getConfig()->getConfigParam('bTRWMailRepeatValidEMailHost')) {
            $oInputValidator->checkEMailHost($sEMail);
        }

        // throwing first validation error
        if ($oError = Registry::getInputValidator()->getFirstValidationError()) {
            throw $oError;
        }
    }
}
