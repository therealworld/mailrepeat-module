OXID6 MailRepeat Module
======

![vendor-logo the-real-world.de](picture.png)

### Features

* Add a second email-input field in the registration- and the order-address-dialog
* Prevents input via Copy and Paste (client side via jquery)
* Checks if both e-mail addresses are identical
* Optional: DNS check of the e-mail host

### Themeing

* 100% Compatible for WAVE or FLOW Standard-Theme
* 100% Compatible for child-themes based on WAVE or FLOW Standard-Theme
* The use of the module template blocks is optional. So if necessary, the own theme can be used completely.
* For using in your own-Theme, visit the tpl-Blocks and copy the part of the second mail-adress to your theme

### Options in Module-Backend

* activate the Module
* Optional: Use Template-Blocks or not
* Optional: Check the DNS of the e-mail host

### Module installation via composer

In order to install the module via composer run one of the following commands in commandline in your shop base directory
(where the shop's composer.json file resides).
* **composer require therealworld/emailrepeat-module** to install the actual version compatible with OXID6

## Bugs and Issues

If you experience any bugs or issues, please report them in on https://bitbucket.org/therealworld/mailrepeat-module/issues.

