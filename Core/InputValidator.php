<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

namespace TheRealWorld\MailRepeatModule\Core;

use OxidEsales\Eshop\Core\Exception\UserException;
use OxidEsales\Eshop\Core\Registry;

/**
 * Class for validating input.
 *
 * @mixin \OxidEsales\Eshop\Core\InputValidator
 */
class InputValidator extends InputValidator_parent
{
    /**
     * Checks if email (used as login) is not the same like the repeated Mail.
     *
     * @param string $sEMail       user email
     * @param string $sEMailRepeat user email repeat
     */
    public function checkEMailRepeat(string $sEMail, string $sEMailRepeat): void
    {
        //  emails do not match ?
        if ($sEMail !== $sEMailRepeat) {
            $oEx = oxNew(UserException::class);
            $oEx->setMessage(Registry::getLang()->translateString('TRWMAILREPEAT_ERROR_MESSAGE_EMAIL_DO_NOT_MATCH'));

            $this->_addValidationError('oxuser__oxusername', $oEx);
        }
    }

    /**
     * Checks if the email host is valid.
     *
     * @param string $sEMail user email
     */
    public function checkEMailHost(string $sEMail): void
    {
        $bError = true;
        $sHost = null;

        // validate email syntax
        if (filter_var($sEMail, FILTER_VALIDATE_EMAIL) !== false) {
            $aHost = explode('@', $sEMail);

            // validate dns of emailhost
            if (
                ($sHost = array_pop($aHost)) && checkdnsrr(
                    idn_to_ascii(
                        $sHost,
                        IDNA_NONTRANSITIONAL_TO_ASCII
                    )
                )
            ) {
                $bError = false;
            }
        }

        if ($bError) {
            $sHost = (is_null($sHost) ? $sEMail : $sHost);
            $oEx = oxNew(UserException::class);
            $oEx->setMessage(sprintf(
                Registry::getLang()->translateString('TRWMAILREPEAT_ERROR_MESSAGE_EMAIL_HOST_NOT_VALID'),
                $sHost
            ));

            $this->_addValidationError('oxuser__oxusername', $oEx);
        }
    }
}
