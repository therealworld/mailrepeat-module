<?php

/**
 * @author  Mario Lorenz, www.the-real-world.de
 * @license https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
 */

declare(strict_types=1);

// Metadata version.

use OxidEsales\Eshop\Application\Component\UserComponent as OxUserComponent;
use OxidEsales\Eshop\Application\Model\User as OxUser;
use OxidEsales\Eshop\Core\InputValidator as OxInputValidator;
use OxidEsales\Eshop\Core\ViewConfig as OxViewConfig;
use TheRealWorld\MailRepeatModule\Application\Component\UserComponent;
use TheRealWorld\MailRepeatModule\Application\Model\User;
use TheRealWorld\MailRepeatModule\Core\InputValidator;
use TheRealWorld\MailRepeatModule\Core\ViewConfig;
use TheRealWorld\ToolsPlugin\Core\ToolsModuleVersion;

$sMetadataVersion = '2.1';

/**
 * Module information.
 */
$aModule = [
    'id'    => 'trwmailrepeat',
    'title' => [
        'de' => 'the-real-world - E-Mail wiederholen während Registrierung',
        'en' => 'the-real-world - Repeat e-mail during registration',
    ],
    'description' => [
        'de' => 'fügt ein weiteres E-Mail Feld während der Registrierung ein, das indentisch befüllt werden muss',
        'en' => 'inserts another e-mail field during registration, which must be filled in the same way',
    ],
    'thumbnail' => 'picture.png',
    'version'   => ToolsModuleVersion::getModuleVersion('trwmailrepeat'),
    'author'    => 'Mario Lorenz',
    'url'       => 'https://www.the-real-world.de',
    'email'     => 'mario_lorenz@the-real-world.de',
    'events'    => [
        'onActivate'   => '\TheRealWorld\MailRepeatModule\Core\MailRepeatEvents::onActivate',
        'onDeactivate' => '\TheRealWorld\MailRepeatModule\Core\MailRepeatEvents::onDeactivate',
    ],
    'extend' => [
        // Component
        OxUserComponent::class => UserComponent::class,
        // Core
        OxViewConfig::class     => ViewConfig::class,
        OxInputValidator::class => InputValidator::class,
        // Model
        OxUser::class => User::class,
    ],
    'blocks' => [
        [
            'template' => 'form/fieldset/user_account.tpl',
            'block'    => 'user_account_username',
            'file'     => 'Application/views/blocks/form/fieldset/user_account_username.tpl',
        ],
        [
            'template' => 'form/fieldset/user_noaccount.tpl',
            'block'    => 'user_noaccount_email',
            'file'     => 'Application/views/blocks/form/fieldset/user_noaccount_email.tpl',
        ],
    ],
    'settings' => [
        // These options are invisible because the "group" option is null
        [
            'group' => 'trwmailrepeat',
            'name'  => 'bTRWMailRepeatActive',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwmailrepeat',
            'name'  => 'bTRWMailRepeatUseBlocks',
            'type'  => 'bool',
            'value' => true,
        ],
        [
            'group' => 'trwmailrepeat',
            'name'  => 'bTRWMailRepeatValidEMailHost',
            'type'  => 'bool',
            'value' => true,
        ],
    ],
];
